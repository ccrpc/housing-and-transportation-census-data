# https://api.census.gov/data/2022/acs/acs5?get=NAME,B01001_001E&for=block%20group:*&in=state:06%20county:073&key=YOUR_KEY_GOES_HERE

from operator import index # target file 
import requests
import pandas as pd
import geopandas as gpd 
from shapely.geometry import Polygon, MultiPolygon

variables_input='DP05_0001E,DP05_0040E,DP05_0041E,DP05_0042E,DP05_0043E'

county_code='019'
census_key = '43531ca537bf8acaa16d1e483938fd33378342ab'
census_year=2022
# list_of_school_district=''

url_census=f'https://api.census.gov/data/2022/acs/acs5/profile?get=NAME,{variables_input}&for=tract:*&in=state:17&in=county:019&key={census_key}'

response_census = requests.get(url_census)

if response_census.status_code == 200:
    data = response_census.json()
    df = pd.DataFrame(data[1:], columns=data[0])
    df.to_csv('DP05__American_Indian_And_Alaska_Native________H&T_3.csv', index=False)
    print(df)
else:
    print(response_census.status_code)
    print(response_census.text)